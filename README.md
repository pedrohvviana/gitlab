# Gitlab

Treinamento Linuxtips Gitlab 

Comandos Iniciais

###Day-1
```bash
 - Entendemos o que é o Git
 - Entendemos o que é o Working Dir, Index, Head
 - Entendemos o que é o Gitlab
 - Como criar um Grupo no Gitlab
 - Como criar um Repositório no Git
 - Aprendemos os comandos básicos para manipulação de arquivos e diretórios no Git
 - Como criar uma branch
 - Como criar uma Merge Request
 - Como fazer o merge na Master/Main
 - Como associar um repo local com um repo remoto
 - Como importar um repo do GitHub para o Gitlab
 - Mudamos a branch padrão para main 
```
